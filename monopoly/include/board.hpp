#ifndef BOARD_HPP
#define BOARD_HPP

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

#include <vector>
#include <map>
#include <deque>
#include <string>
#include <iostream>

#include <QObject>
#include <QFile>
#include <QDir>
#include <QRectF>
#include <QPainter>
#include <QStyleOption>
#include <QGraphicsItem>
#include <QGraphicsScene>
#include <QPainter>
#include <QColor>
#include <QStyleOption>
#include <QGraphicsItemGroup>
#include <QGraphicsPixmapItem>
#include <QRandomGenerator>
#include <QAbstractAnimation>
#include <QVariantList>
#include <QTextStream>
#include <QGraphicsView>
#include "button.hpp"
#include "field.hpp"
#include "card_chance_community.hpp"
#include "rectangle_with_text.hpp"
#include "my_pixmap_graphics_item.hpp"
#include "card_property.hpp"
#include "animate_player.hpp"

#define OUTER_BOX 1000
#define INNER_BOX 800
#define FIELD_HEIGHT 100
#define CARD_WIDTH 200
#define CARD_HEIGHT 100
#define CHANCE_CARD_POSITION_X 160
#define CHANCE_CARD_POSITION_Y 210
#define COMMUNITY_CARD_POSITION_X 640
#define COMMUNITY_CARD_POSITION_Y 690

class Board:public QObject
{
    Q_OBJECT

public:

    Board(); 
    ~Board();

    QGraphicsScene *scene_;
    QGraphicsView* view_;

    void set_scene(QGraphicsScene *scene);
    void display_table();
    void load();
    void load_special_fields();
    void set_dice_numbers();
    void add_special_buttons(QGraphicsRectItem *rect_bottom, QGraphicsRectItem *rect_left, QGraphicsRectItem *rect_upper, QGraphicsRectItem *rect_right);
    void add_buttons(QGraphicsItem *rect_outer);

    std::map<unsigned int, Field*> get_fields() const {return fields_;}
    std::vector<Button*> get_buttons() const {return buttons_fields_;}
    Button* get_button_by_name(QString& name) const;
    std::deque<Card_Chance_Community*> get_chance_cards() const {return chance_cards_;}
    std::deque<Card_Chance_Community*> get_community_cards() const {return community_cards_;}

    unsigned get_field_id(const QString &name) const;
    Field* get_field_by_id(unsigned field_id) const;
    QGraphicsScene *getScene() const {return scene_;}


    void return_card(Card_Chance_Community* c);
    Card_Chance_Community* take_card(const Card_category c);
    Animate_Player *getCurrent_player() const {return current_player_;}
    void setCurrent_player(Animate_Player *current_player) {current_player_ = current_player;}
    std::pair<int, int> get_generated_dice_numbers() {return std::pair(dice1_,dice2_);}

    std::vector<Animate_Player *> getPlayers() const {return players_;}
    Animate_Player* getCurrentPlayer(int index) const;
    void addPlayer(Animate_Player *p) {players_.push_back(p);}




signals:

    void dice_animation_finished();

public slots:

    void animate_dice();
    void show_property_card(Card_Property *c) {scene_->addItem(c);}
    void hide_property_card(Card_Property *c) {scene_->removeItem(c);}
    void owned_property_card(QString);

private:
    
    std::map<unsigned, Field*> fields_;

    std::deque<Card_Chance_Community*> chance_cards_;
    std::deque<Card_Chance_Community*> community_cards_;

    QJsonArray open_and_test_json_file(QFile &file_obj);
    QJsonObject test_element(QJsonValue &element);
    Field* read_property(const QVariantMap &json);
    Field* read_field(const QVariantMap &json);
    Card_Chance_Community* readCard_Chance_Community(const QVariantMap &json);
    QGraphicsPixmapItem*  rotateCard(QGraphicsPixmapItem *card);

    int dice1_ = 0;
    int dice2_ = 0;
    std::vector<Button*> buttons_fields_;
    Animate_Player* current_player_;
    std::vector<Animate_Player*> players_;

};

#endif // BOARD_HPP
