#ifndef NOTIFICATION_HPP
#define NOTIFICATION_HPP
#include <QGraphicsItem>
#include <QPainter>
#include <QStyleOptionGraphicsItem>
#include <QRectF>

class Notification: public QGraphicsItem{

public:
    Notification(int x,int y,const QString msg="");

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
               QWidget *widget) override;

    QRectF boundingRect() const override{
        return QRectF(x_,y_, this->width, this->height);
    }



    void setWidth(const int width){this->width = width;}
    void setHeight(const int height){this->height= height;}
    void setPositionX(const int x){this->x_ = x;}
    void setPositionY(const int y){this->y_= y;}


public slots:
    void setMsg(QString& new_msg){msg_box = new_msg;}

private:
    QString msg_box;
    int height = 150;
    int width = 845;
    int x_;
    int y_;
    QRectF bounding = this->boundingRect();
    QGraphicsTextItem *text;
    int padding = 10;
    QColor notif_color;
    QColor bound_color;
    int pen_width = 2;

};


#endif // NOTIFICATION_HPP
