#ifndef BUTTON_HPP
#define BUTTON_HPP

#include <QGraphicsItem>
#include <QGraphicsScene>
#include <QPainter>
#include <QStyleOption>

#include <iostream>

#include "field.hpp"
#include "card_property.hpp"

#define FIELD_WIDTH 89
#define FIELD_HEIGHT 100
#define COLOR_FIELD 20

class Button :public QObject, public QGraphicsItem
{
    Q_OBJECT

public:

    Button(unsigned id, QString text, Field_kind kind, qint64 rotation, qint64 x_pos, qint64 y_pos, QGraphicsItem *parent);
    Button(Field *field, QColor style, qint64 rotation, qint64 x_pos, qint64 y_pos, Card_Property *card_prop, QGraphicsItem *parent);
    ~Button();

    QRectF boundingRect() const override;
    QPainterPath shape() const override;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;
    void mousePressEvent(QGraphicsSceneMouseEvent *event) override;
    void hoverEnterEvent(QGraphicsSceneHoverEvent *event) override;
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event) override;


    QString getText() const {return text_;}
    QColor getStyle() const {return style_;}
    qint64 getX() const {return x_pos_;}
    qint64 getY() const {return y_pos_;}
    qint64 get_rotation() const {return rotation_;}
    unsigned get_id() const {return id_;}
    qint64 get_price() const {return price_;}
    qint64 get_rent() const {return rent_;}
    void set_rent(qint64 rent) {rent_ = rent;}
    Field_kind get_kind() const {return kind_;}
    Owner get_owner() const {return o_;}
    void set_owner(Owner o) {o_ = o;}
    Card_Property *get_card_prop() const {return card_prop_;}
    bool get_show_or_hide() const {return show_or_hide;}
    void set_show_or_hide(bool value) {show_or_hide = value;}


signals:

    void show_property_card(Card_Property *c);
    void hide_property_card(Card_Property *c);
    void hide_property_card();

private:

    bool show_or_hide = true;
    Field *field_ = nullptr;
    Owner o_;
    unsigned id_;
    QString text_ = "";
    qint64 price_ = 0;
    qint64 rent_ = 0;
    Field_kind kind_;
    QColor style_ = QColor(0, 0, 0);
    qint64 rotation_;
    qint64 x_pos_;
    qint64 y_pos_;
    Card_Property *card_prop_;

};

#endif // BUTTON_HPP
