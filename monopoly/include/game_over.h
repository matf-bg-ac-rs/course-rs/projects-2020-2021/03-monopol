#ifndef GAME_OVER_H
#define GAME_OVER_H
#include <QWidget>
#include <QFrame>
#include <QPushButton>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QFrame>
#include <QSpacerItem>
#include <QRect>

#include "welcome.hpp"

#include <iostream>

QT_BEGIN_NAMESPACE
namespace Ui {
class Game_over;
}
QT_END_NAMESPACE

class Game_over : public QWidget
{
    Q_OBJECT

public:
    explicit Game_over(std::vector<std::pair<QString, int> > score, QWidget *parent = nullptr);
    ~Game_over();

public slots:
    void exit_clicked();
protected:
    void resizeEvent(QResizeEvent *event);

private:
    void set_frame();
    void set_content(std::vector<std::pair<QString, int> > score);
    void set_score(QVBoxLayout &vlayout, std::vector<std::pair<QString, int>> score);
    void add_buttons(QVBoxLayout &vlayout, QHBoxLayout &hlayout);


    Ui::Game_over *ui;
    QFrame *frame;
    QPixmap border = QPixmap(":/images/chalkboard.jpg");
    double border_width = border.width();
    double border_height = border.height();
    QPushButton *exit;
};

#endif // GAME_OVER_H
