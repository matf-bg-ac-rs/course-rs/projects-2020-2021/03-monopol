#include "../include/game.hpp"

void Game::tell_about_going_to_jail()
{
    if(curr_player_->get_id()==0){
        QGraphicsTextItem *go_to_jail_text = new QGraphicsTextItem("You've stepped on GO TO JAIL! Bad luck!");
        go_to_jail_text->setTextWidth(210);
        go_to_jail_text->setPos(1640, 575);
        board_->scene_->addItem(go_to_jail_text);
    }
    go_to_jail(*curr_player_);
}
void Game::remove_tell_about_going_to_jail()
{
    QGraphicsItem *go_to_jail_text = board_->view_->itemAt(1640, 575);
    board_->scene_->removeItem(go_to_jail_text);
    delete go_to_jail_text;
}
void Game::process_player_being_in_jail()
{
    if (curr_player_->check_jail())
    {
        if(curr_player_->check_jail_card())
        {
            emit player_owns_jail_card();
        }
        else
        {
            emit player_doesnt_own_jail_card();
        }
    }
    else
    {
     emit processing_about_jail_finished();
    }
}
void Game::ask_about_using_jail_card()
{
    QGraphicsTextItem *question_jail_card = new QGraphicsTextItem("Do you want to use your get out of jail for free card?");
    question_jail_card->setTextWidth(210);
    question_jail_card->setPos(1640, 575);

    Rectangle_with_text *pay_for_jail = new Rectangle_with_text("No, I want to pay 50€",   Qt::darkRed,1680, 750,100,30);
    Rectangle_with_text *use_card = new Rectangle_with_text("Yes, I want to use the card",Qt::darkGreen,1680, 650,100,30);

    board_->scene_->addItem(question_jail_card);
    board_->scene_->addItem(pay_for_jail);
    board_->scene_->addItem(use_card);

    QObject::connect(pay_for_jail, SIGNAL(i_want_to_pay_for_jail()), this, SLOT(pay_even_with_card()));

    QObject::connect(use_card, SIGNAL(i_want_to_use_the_card()), this, SLOT(use_card_to_get_out_of_jail()));
    QObject::connect(use_card, SIGNAL(i_want_to_use_the_card()), this, SLOT(remove_ask_about_jail_card()));
}
void Game::remove_ask_about_jail_card()
{
    QGraphicsItem *question = board_->view_->itemAt(1640, 575);
    board_->scene_->removeItem(question);
    delete question;

    QGraphicsItem* pay_option = board_->view_->itemAt(1680, 650);
    board_->scene_->removeItem(pay_option);
    delete pay_option;

    QGraphicsItem* card_option = board_->view_->itemAt(1680, 750);
    board_->scene_->removeItem(card_option);
    delete card_option;
}
void Game::pay_to_get_out_of_jail()
{
    if(curr_player_->pay_to_bank(50))
    {
        bank_.collect(50);
        change_budget_rect();
        emit processing_about_jail_with_work_finished();
    }
    else
    {
        QGraphicsTextItem *jail_mortgage_text = new QGraphicsTextItem("You have to mortgage some property because you don't have 50€ "
                                                                      "to get out of jail. Please choose a property to mortgage on the "
                                                                      "list of properties you own by doing a double mouse click on it! "
                                                                      "If you don't mortgage any property you'll go bankrupt.");
        jail_mortgage_text->setTextWidth(200);
        jail_mortgage_text->setPos(1640, 575);
        board_->scene_->addItem(jail_mortgage_text);

        Rectangle_with_text *not_mortgaging = new Rectangle_with_text("I don't want to mortgage property",Qt::darkRed,1680, 750,100,30);
        board_->scene_->addItem(not_mortgaging);

        QObject::connect(not_mortgaging, SIGNAL(im_not_mortgaging()), this, SLOT(remove_not_mortgaging()));

        curr_player_->disable_all_rectangles_with_text(false);

        QObject::connect(this, SIGNAL(send_field_to_mortgage(Field&,Player&)), this, SLOT(mortgage_property(Field&,Player&)));
        QObject::connect(this, SIGNAL(finished_with_mortgaging_property(int)), this, SLOT(pay_after_mortgaging(int)));
        QObject::connect(this, SIGNAL(finished_with_paying_after_mortgaging()), this, SLOT(remove_pay_to_get_out_jail()));
    }

    curr_player_->set_jail(false);
}
void Game::pay_even_with_card()
{
    QGraphicsItem *question = board_->view_->itemAt(1640, 575);
    board_->scene_->removeItem(question);
    delete question;

    QGraphicsItem* pay_option = board_->view_->itemAt(1680, 650);
    board_->scene_->removeItem(pay_option);
    delete pay_option;

    QGraphicsItem* card_option = board_->view_->itemAt(1680, 750);
    board_->scene_->removeItem(card_option);
    delete card_option;

    if(curr_player_->pay_to_bank(50))
    {
        bank_.collect(50);
        change_budget_rect();
        emit processing_about_jail_with_work_finished();
    }
    else
    {
        QGraphicsTextItem *jail_mortgage_text = new QGraphicsTextItem("You have to mortgage some property because you don't have 50€ "
                                                                      "to get out of jail. Please choose a property to mortgage on the "
                                                                      "list of properties you own by doing a double mouse click on it! "
                                                                      "If you don't mortgage any property you'll go bankrupt.");
        jail_mortgage_text->setTextWidth(200);
        jail_mortgage_text->setPos(1640, 575);
        board_->scene_->addItem(jail_mortgage_text);

        Rectangle_with_text *not_mortgaging = new Rectangle_with_text("I don't want to mortgage property",Qt::darkRed,1680, 750,100,30);
        board_->scene_->addItem(not_mortgaging);

        QObject::connect(not_mortgaging, SIGNAL(im_not_mortgaging()), this, SLOT(remove_not_mortgaging()));

        curr_player_->disable_all_rectangles_with_text(false);

        QObject::connect(this, SIGNAL(send_field_to_mortgage(Field&,Player&)), this, SLOT(mortgage_property(Field&,Player&)));
        QObject::connect(this, SIGNAL(finished_with_mortgaging_property()), this, SLOT(pay_after_mortgaging()));
        QObject::connect(this, SIGNAL(finished_with_paying_after_mortgaging()), this, SLOT(remove_pay_to_get_out_jail()));
    }

    curr_player_->set_jail(false);
}
void Game::use_card_to_get_out_of_jail()
{
    Field_kind card_from = curr_player_->unset_jail_card();
    return_jail_card(card_from);
    set_jail_card(card_from, false, nullptr);

    QGraphicsRectItem* jail_card = qgraphicsitem_cast<QGraphicsRectItem*>(board_->view_->itemAt(1180, 600));
    board_->scene_->removeItem(jail_card);
    jail_card->setBrush(Qt::darkRed);
    board_->scene_->addItem(jail_card);

    emit processing_about_jail_with_work_finished();
}
void Game::remove_pay_to_get_out_jail()
{
    QGraphicsItem *jail_mortgage_text = board_->view_->itemAt(1640, 575);
    board_->scene_->removeItem(jail_mortgage_text);
    delete jail_mortgage_text;

    QGraphicsItem* not_mortgaging = board_->view_->itemAt(1680, 750);
    board_->scene_->removeItem(not_mortgaging);
    delete not_mortgaging;

    curr_player_->set_needed_money(0);
    emit processing_about_jail_with_work_finished();
}
