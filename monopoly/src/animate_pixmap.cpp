#include "../include/animate_pixmap.hpp"

Animate_Pixmap::Animate_Pixmap(QGraphicsPixmapItem *parent)
    :QGraphicsPixmapItem(parent),
      parent_(parent)
{
    animation_ = new QPropertyAnimation(this, "pos", this);
}

QPointF Animate_Pixmap::pos() const {return parent_->offset();}

QPropertyAnimation *Animate_Pixmap::animaton() const {return animation_;}

void Animate_Pixmap::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *) {
    Q_UNUSED(painter)
    Q_UNUSED(option);
}

QRectF Animate_Pixmap::boundingRect() const {return QRectF();}

void Animate_Pixmap::setPos(QPointF &point) {parent_->setOffset(point);}
