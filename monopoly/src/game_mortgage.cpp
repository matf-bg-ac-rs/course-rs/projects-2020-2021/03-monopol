#include "../include/game.hpp"

void Game::remove_not_mortgaging()
{
    QGraphicsItem *you_have_to_mortgage_text = board_->view_->itemAt(1640, 575);
    board_->scene_->removeItem(you_have_to_mortgage_text);
    delete you_have_to_mortgage_text;

    QGraphicsItem* not_mortgaging = board_->view_->itemAt(1680, 750);
    board_->scene_->removeItem(not_mortgaging);
    delete not_mortgaging;

    curr_player_->set_needed_money(0);
    be_bankrupt(curr_player_);
    curr_player_->set_bankrupted(true);
    emit player_bankrupted();
}
void Game::got_mortgage_to_get_out_of_jail(QString name_of_clicked_property)
{
    unsigned int f_id = board_->get_field_id(name_of_clicked_property);
    Field* prop_field = board_->get_field_by_id(f_id);

    emit send_field_to_mortgage(*prop_field, *curr_player_);
}
void Game::pay_after_mortgaging(int money)
{
    if(Owner::BANK == curr_field_->get_owner())
    {
        if(curr_player_->pay_to_bank(money))
        {
            notification_string_.append(  curr_player_->get_name() + " paid bank " +QString::number(money) + "€ after mortgaging.");
            notification_string_.append("Current budget of " + curr_player_->get_name() + " is " +QString::number(curr_player_->get_budget()) + "€.");
            bank_.collect(money);
        }
        else
        {
            curr_player_->set_bankrupted(true);
            be_bankrupt(curr_player_);
            if(curr_player_->get_id()==0)
                emit player_bankrupted();
        }
    }
    else
    {
        unsigned sum = curr_field_->get_rent();
        Owner owner = curr_field_->get_owner();
        Player *p = get_player_by_id(owner);

        if(curr_player_->pay_to_player(p, sum))
        {
         notification_string_.append(  curr_player_->get_name() + " paid " + money + "€ after mortgaging.");
         notification_string_.append(  curr_player_->get_name() + " current budget is " +curr_player_->get_budget() + "€.");

         if(p->get_id()==0){
             Rectangle_with_text *budget_rect = qgraphicsitem_cast<Rectangle_with_text*>(board_->view_->itemAt(1055, 570));
             board_->scene_->removeItem(budget_rect);

             QString s = QString::number(curr_player_->get_budget());
             s.append("€");
             budget_rect->set_text(s);
             board_->scene_->addItem(budget_rect);

          }
        }
        else
        {
            curr_player_->set_bankrupted(true);
            be_bankrupt(curr_player_);
            notification_string_.append(  curr_player_->get_name() + " bankrupted. ");
            if(curr_player_->get_id()==0)
                 emit player_bankrupted();
        }
    }
    curr_player_->set_needed_money(0);
    if(curr_player_->get_id()==0){
        change_budget_rect();
        emit finished_with_paying_after_mortgaging();
    }

}
void Game::ask_about_lifting_mortgage()
{
    QGraphicsTextItem *question = new QGraphicsTextItem("Do you want to lift the mortgage?");
    question->setTextWidth(-1);
    question->setPos(1640, 575);

    Rectangle_with_text *yes_option = new Rectangle_with_text("I want to lift the mortgage",   Qt::darkGreen,1680, 650,100,30);
    Rectangle_with_text *no_option = new Rectangle_with_text("I don't want to lift the mortgage",Qt::darkRed,1680, 750,100,50);

    board_->scene_->addItem(question);
    board_->scene_->addItem(yes_option);
    board_->scene_->addItem(no_option);

    QObject::connect(yes_option, SIGNAL(i_want_to_lift_the_mortgage()), this, SLOT(lifting_mortgage()));
    QObject::connect(no_option, SIGNAL(i_dont_want_to_lift_the_mortgage()), this, SLOT(not_lifting_mortgage()));
}
void Game::lifting_mortgage()
{
    QGraphicsItem *question = board_->view_->itemAt(1640, 575);
    board_->scene_->removeItem(question);
    delete question;

    QGraphicsItem* yes_option = board_->view_->itemAt(1680, 650);
    board_->scene_->removeItem(yes_option);
    delete yes_option;

    QGraphicsItem* no_option = board_->view_->itemAt(1680, 750);
    board_->scene_->removeItem(no_option);
    delete no_option;

    int mortgage_w_interest = calculate_mortgage(curr_field_->get_mortgage());
    if(curr_player_->pay_mortgage(mortgage_w_interest))
    {
        bank_.collect(mortgage_w_interest);
        curr_field_->set_mortgage(false);

        change_budget_rect();

        auto rect_with_text = curr_player_->find_rectangle_with_text(curr_field_->get_name());
        QString old_color = curr_field_->get_color();
        if(rect_with_text != nullptr)
        {
           board_->scene_->removeItem(rect_with_text);
           rect_with_text->set_color(old_color);
           board_->scene_->addItem(rect_with_text);

           QGraphicsTextItem *notif = new QGraphicsTextItem("Mortgage is lifted!");
           notif->setTextWidth(-1);
           notif->setPos(1640, 575);
           board_->scene_->addItem(notif);
        }
        else
        {
            std::cout<<"There is no rectangle with text of the current field you are on"<<std::endl;
        }

        emit enough_money_to_lift_the_mortgage();
    }
    else
    {
        QGraphicsTextItem *notif = new QGraphicsTextItem("You don't have enough money to lift the mortgage");
        notif->setTextWidth(210);
        notif->setPos(1640, 575);
        board_->scene_->addItem(notif);

        Rectangle_with_text *okay = new Rectangle_with_text("Okay",   Qt::darkGreen,1680, 650,100,30);
        board_->scene_->addItem(okay);

        QObject::connect(okay, SIGNAL(okay_i_dont_have_money_to_buy()), this, SIGNAL(not_enough_money_to_lift_the_mortgage()));
    }
}
void Game::not_lifting_mortgage()
{
    QGraphicsItem *question = board_->view_->itemAt(1640, 575);
    board_->scene_->removeItem(question);
    delete question;

    QGraphicsItem* yes_option = board_->view_->itemAt(1680, 650);
    board_->scene_->removeItem(yes_option);
    delete yes_option;

    QGraphicsItem* no_option = board_->view_->itemAt(1680, 750);
    board_->scene_->removeItem(no_option);
    delete no_option;
}
void Game::remove_notif_about_lifting()
{
    QGraphicsItem *notif = board_->view_->itemAt(1640, 575);
    board_->scene_->removeItem(notif);
    delete notif;
}
void Game::change_color_to_black_when_mortgaged(QString name)
{
    auto property= curr_player_->find_rectangle_with_text(name);
    if(property!=nullptr){
        board_->scene_->removeItem(property);
        property->set_color(Qt::black);
        board_->scene_->addItem(property);
        curr_player_->disable_all_rectangles_with_text(true);
    }
    else{
        std::cout<<"There is no rectangle with text with the name: "<<name.toStdString()<<std::endl;
    }
}
void Game::remove_mortgage_question(){

    QGraphicsItem *mortgage_text = board_->view_->itemAt(1640, 575);
    board_->scene_->removeItem(mortgage_text);
    delete mortgage_text;

    QGraphicsItem* not_mortgaging = board_->view_->itemAt(1680, 750);
    board_->scene_->removeItem(not_mortgaging);
    delete not_mortgaging;
}
