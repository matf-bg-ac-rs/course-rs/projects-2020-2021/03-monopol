#include "../include/animate_player.hpp"
#include "../include/animate_pixmap.hpp"

#include<iostream>
#include <QtGlobal>

Animate_Player::Animate_Player(QString str, QGraphicsPixmapItem* token_pixmap)
        : str_(str), token_pixmap_(token_pixmap)
{
    x_ = 900;
    qint16 adjust = 0;
    if(!str_.contains("ship"))
    {
        adjust = 15;
    }
    y_ = 910+adjust;
    position_ = 0;
}

void Animate_Player::animate_player()
{
    Animate_Pixmap* animator = new Animate_Pixmap(token_pixmap_);
    qint16 adjust = 0;
    if(!str_.contains("ship"))
    {
        adjust = 15;
    }
    animate_advancing(animator, adjust);
}

void Animate_Player::setPosition(const qint32 &position) {position_ = position;}

void Animate_Player::hide_token() {token_pixmap_->hide();}

qint32 Animate_Player::position() const {return position_;}

qint16 Animate_Player::advance() const {return advance_;}

void Animate_Player::setAdvance(const qint16 &advance) {advance_ = advance;}

//this function animates player after the dice are thrown or when player has to move because of a certain chance/community card
void Animate_Player::animate_advancing(Animate_Pixmap *animator, qint16 adjust)
{
    qint32 new_x = 0;
    qint32 new_y = 0;
    qint32 new_position = position_ + advance();
    new_position = new_position%40;

    animator->animaton()->setDuration(abs(advance()) * 500);
    //---------------------bottom-------------------------------------------
    if(0 <= position_ && position_ <= 9 && 0 <= new_position && new_position <= 10)
    {
        new_x = 900 - (88*new_position);
        new_y = 910 + adjust;
        animator->animaton()->setStartValue(QPointF(x_, y_));
        animator->animaton()->setEndValue(QPointF(new_x, new_y));
        animator->animaton()->start();
        x_ = new_x;
        y_ = new_y;
        position_ = new_position;
        QObject::connect(animator->animaton(), SIGNAL(finished()), this, SIGNAL(finished_animation_for_player()));
    }
    else if(0 <= position_ && position_ <= 9 && 11 <= new_position && new_position <= 20){
        new_x = 20;
        new_y = 812;
        new_y -= 88*(new_position-11);
        if(new_position == 20)
            new_y -= 10 - adjust;
        animator->animaton()->setStartValue(QPointF(x_, y_));
        animator->animaton()->setKeyValueAt(0.5, QPointF(20, 910+adjust));
        animator->animaton()->setEndValue(QPointF(new_x, new_y));
        animator->animaton()->start();
        x_ = new_x;
        y_ = new_y;
        position_=new_position;
        QObject::connect(animator->animaton(), SIGNAL(finished()), this, SIGNAL(finished_animation_for_player()));

    }
    else if(0 <= position_ && position_ <= 9 && 21 <= new_position && new_position <= 30)
    {
        new_y = 10+adjust;
        if(new_position == 21)
        {
            new_x = 108;
        }
        else
        {
            new_x = 108 + (new_position%21)*88;
        }
        animator->animaton()->setStartValue(QPointF(x_, y_));
        animator->animaton()->setKeyValueAt(0.22, QPointF(20, 910+adjust));
        animator->animaton()->setKeyValueAt(0.88, QPointF(20, 10+adjust));
        animator->animaton()->setEndValue(QPointF(new_x, new_y));
        animator->animaton()->start();
        x_=new_x;
        y_=new_y;
        position_=new_position;
        QObject::connect(animator->animaton(), SIGNAL(finished()), this, SIGNAL(finished_animation_for_player()));
    }
    else if(0 <= position_ && position_ <= 9 && 31 <= new_position && new_position <= 39)
    {
        new_x = 900;
        if(new_position == 31)
        {
            new_y = 108;
        }
        else
        {
            new_y = 108 + (new_position%31)*88;
        }
        animator->animaton()->setStartValue(QPointF(x_, y_));
        animator->animaton()->setKeyValueAt(0.25, QPointF(20, 910+adjust));
        animator->animaton()->setKeyValueAt(0.50, QPointF(20, 10+adjust));
        animator->animaton()->setKeyValueAt(0.75, QPointF(900, 10+adjust));
        animator->animaton()->setEndValue(QPointF(new_x, new_y));
        animator->animaton()->start();
        x_=new_x;
        y_=new_y;
        position_=new_position;
        QObject::connect(animator->animaton(), SIGNAL(finished()), this, SIGNAL(finished_animation_for_player()));
    }
    //-------------------------left---------------------------------------------
    else if(10 <= position_ && position_ <= 19 && 10 <= new_position && new_position <= 20)
    {
        new_x = 20;
        new_y = 812;
        new_y -= 88*(new_position-11);
        if(new_position == 20)
            new_y -= 10 - adjust;
        animator->animaton()->setStartValue(QPointF(x_, y_));
        animator->animaton()->setEndValue(QPointF(new_x, new_y));
        animator->animaton()->start();
        x_ = new_x;
        y_ = new_y;
        position_=new_position;
        QObject::connect(animator->animaton(), SIGNAL(finished()), this, SIGNAL(finished_animation_for_player()));
    }
    else if(10 <= position_ && position_ <= 19 && 21 <= new_position && new_position <= 30)
    {
        new_y = 10+adjust;
        if(new_position == 31)
        {
            new_x = 108;
        }
        else
        {
            new_x = 108 + (new_position%21)*88;
        }

        animator->animaton()->setStartValue(QPointF(x_, y_));
        animator->animaton()->setKeyValueAt(0.5, QPointF(20, 10+adjust));
        animator->animaton()->setEndValue(QPointF(new_x, new_y));
        animator->animaton()->start();
        x_ = new_x;
        y_ = new_y;
        position_=new_position;
        QObject::connect(animator->animaton(), SIGNAL(finished()), this, SIGNAL(finished_animation_for_player()));
    }
    else if(10 <= position_ && position_ <= 19 && 31 <= new_position && new_position <= 39)
    {
        new_x = 900;
        if(new_position == 31)
        {
            new_y = 108;
        }
        else
        {
            new_y = 108 + (new_position%31)*88;
        }
        animator->animaton()->setStartValue(QPointF(x_, y_));
        animator->animaton()->setKeyValueAt(0.1, QPointF(20, 10+adjust));
        animator->animaton()->setKeyValueAt(0.9, QPointF(900, 10+adjust));
        animator->animaton()->setEndValue(QPointF(new_x, new_y));
        animator->animaton()->start();
        x_=new_x;
        y_=new_y;
        position_=new_position;
        QObject::connect(animator->animaton(), SIGNAL(finished()), this, SIGNAL(finished_animation_for_player()));
    }
    else if(10 <= position_ && position_ <= 19 && 0 <= new_position && new_position <= 9)
    {
        new_y = 910 + adjust;
        new_x = 900 - (new_position%40)*88;

        animator->animaton()->setStartValue(QPointF(x_, y_));
        animator->animaton()->setKeyValueAt(0.25, QPointF(20, 10+adjust));
        animator->animaton()->setKeyValueAt(0.50, QPointF(900, 10+adjust));
        animator->animaton()->setKeyValueAt(0.75, QPointF(900, 910+adjust));
        animator->animaton()->setEndValue(QPointF(new_x, new_y));
        animator->animaton()->start();
        x_=new_x;
        y_=new_y;
        position_=new_position;
        QObject::connect(animator->animaton(), SIGNAL(finished()), this, SIGNAL(finished_animation_for_player()));
    }
    //----------------------top-------------------------
    else if(20 <= position_ && position_ <= 29 && 20 <= new_position && new_position <= 30)
    {
        new_y = 10 + adjust;
        if(new_position == 21)
        {
            new_x = 108;
        }
        else
        {
            new_x = 108 + (new_position%21)*88;
        }
        animator->animaton()->setStartValue(QPointF(x_, y_));
        animator->animaton()->setEndValue(QPointF(new_x, new_y));
        animator->animaton()->start();
        x_ = new_x;
        y_ = new_y;
        position_ = new_position;
        QObject::connect(animator->animaton(), SIGNAL(finished()), this, SIGNAL(finished_animation_for_player()));
    }
    else if(20 <= position_ && position_ <= 29 && 31 <= new_position && new_position <= 39)
    {
        new_x = 900;
        if(new_position == 31)
        {
            new_y = 108;
        }
        else
        {
            new_y = 108 + (new_position%31)*88;
        }
        animator->animaton()->setStartValue(QPointF(x_, y_));
        animator->animaton()->setKeyValueAt(0.5, QPointF(900, 10+adjust));
        animator->animaton()->setEndValue(QPointF(new_x, new_y));
        animator->animaton()->start();
        x_ = new_x;
        y_ = new_y;
        position_=new_position;
        QObject::connect(animator->animaton(), SIGNAL(finished()), this, SIGNAL(finished_animation_for_player()));

    }
    else if(20 <= position_ && position_ <= 29 && 0 <= new_position && new_position <= 10)
    {
        new_y = 910+adjust;
        if(new_position == 0)
        {
            new_x = 900;
        }
        else
        {
            new_x = 900 - (88*new_position);
        }

        animator->animaton()->setStartValue(QPointF(x_, y_));
        animator->animaton()->setKeyValueAt(0.1, QPointF(900, 10+adjust));
        animator->animaton()->setKeyValueAt(0.9, QPointF(900, 910+adjust));
        animator->animaton()->setEndValue(QPointF(new_x, new_y));
        animator->animaton()->start();
        x_=new_x;
        y_=new_y;
        position_=new_position;
        QObject::connect(animator->animaton(), SIGNAL(finished()), this, SIGNAL(finished_animation_for_player()));
    }
    else if(20 <= position_ && position_ <= 29 && 11 <= new_position && new_position <= 19)
    {
        new_x = 20;
        if(new_position == 11)
        {
            new_y = 812;
        }
        else
        {
            new_y = 812 - (new_position%11)*88;
        }
        animator->animaton()->setStartValue(QPointF(x_, y_));
        animator->animaton()->setKeyValueAt(0.25, QPointF(900, 10+adjust));
        animator->animaton()->setKeyValueAt(0.50, QPointF(900, 910+adjust));
        animator->animaton()->setKeyValueAt(0.75, QPointF(20, 910+adjust));
        animator->animaton()->setEndValue(QPointF(new_x, new_y));
        animator->animaton()->start();
        x_=new_x;
        y_=new_y;
        position_=new_position;
        QObject::connect(animator->animaton(), SIGNAL(finished()), this, SIGNAL(finished_animation_for_player()));
    }
    //--------------------right-------------------------
    else if(30 <= position_ && position_ <= 39 && 31 <= new_position && new_position <= 39)
    {

        new_x = 900;
        if(new_position == 31)
        {
            new_y = 108;
        }
        else
        {
            new_y = 108 + (new_position%31)*88;
        }

        animator->animaton()->setStartValue(QPointF(x_, y_));
        animator->animaton()->setEndValue(QPointF(new_x, new_y));
        animator->animaton()->start();
        x_ = new_x;
        y_ = new_y;
        position_ = new_position;
        QObject::connect(animator->animaton(), SIGNAL(finished()), this, SIGNAL(finished_animation_for_player()));
     }
    else if(30 <= position_ && position_ <= 39 && 0 <= new_position && new_position <= 10)
    {
        new_y = 910+adjust;
        if(new_position == 0)
        {
            new_x = 900;
        }
        else
        {
            new_x = 900 - (88*new_position);
        }

        animator->animaton()->setStartValue(QPointF(x_, y_));
        animator->animaton()->setKeyValueAt(0.5, QPointF(900, 910+adjust));
        animator->animaton()->setEndValue(QPointF(new_x, new_y));
        animator->animaton()->start();
        x_ = new_x;
        y_ = new_y;
        position_=new_position;
        QObject::connect(animator->animaton(), SIGNAL(finished()), this, SIGNAL(finished_animation_for_player()));
    }
    else if(30 <= position_ && position_ <= 39 && 11 <= new_position && new_position <= 20)
    {
        new_x = 20;
        new_y = 812;
        new_y -= 88*(new_position-11);
        if(new_position == 20)
            new_y -= 10 - adjust;

        animator->animaton()->setStartValue(QPointF(x_, y_));
        animator->animaton()->setKeyValueAt(0.1, QPointF(900, 910+adjust));
        animator->animaton()->setKeyValueAt(0.9, QPointF(20, 910+adjust));
        animator->animaton()->setEndValue(QPointF(new_x, new_y));
        animator->animaton()->start();
        x_=new_x;
        y_=new_y;
        position_=new_position;
        QObject::connect(animator->animaton(), SIGNAL(finished()), this, SIGNAL(finished_animation_for_player()));
    }
    else if(30 <= position_ && position_ <= 39 && 21 <= new_position && new_position <= 29)
    {
        new_y = 10 + adjust;
        if(new_position == 21)
        {
            new_x = 108;
        }
        else
        {
            new_x = 108 + (new_position%21)*88;
        }
        animator->animaton()->setStartValue(QPointF(x_, y_));
        animator->animaton()->setKeyValueAt(0.25, QPointF(900, 910+adjust));
        animator->animaton()->setKeyValueAt(0.50, QPointF(20, 910+adjust));
        animator->animaton()->setKeyValueAt(0.75, QPointF(20, 10+adjust));
        animator->animaton()->setEndValue(QPointF(new_x, new_y));
        animator->animaton()->start();
        x_=new_x;
        y_=new_y;
        position_=new_position;
        QObject::connect(animator->animaton(), SIGNAL(finished()), this, SIGNAL(finished_animation_for_player()));
    }
    std::cout<<"current coordinates: "<<std::endl;
    std::cout<<x_<<", "<<y_<<", "<<position_<<std::endl;

}

//this function animates player when player gets a Chance card to move three spaces backwards
void Animate_Player::go_back_three_spaces()
{

    Animate_Pixmap *back_animator = new Animate_Pixmap(token_pixmap_);
    qint16 adjust = 0;
    if(!str_.contains("ship"))
    {
        adjust = 15;
    }

    back_animator->animaton()->setDuration(1500);

    if(position_ == 7)
    {
        back_animator->animaton()->setStartValue(QPointF(x_, y_));
        back_animator->animaton()->setEndValue(QPointF(548, 910+adjust));
        back_animator->animaton()->start();
        x_ = 548;
        y_ = 910+adjust;
        position_ = 4;
        QObject::connect(back_animator->animaton(), SIGNAL(finished()), this, SIGNAL(finished_back_animation()));
    }
    else if(position_ == 22)
    {
        back_animator->animaton()->setStartValue(QPointF(x_, y_));
        back_animator->animaton()->setKeyValueAt(0.5, QPointF(20, 10+adjust));
        back_animator->animaton()->setEndValue(QPointF(20, 108));
        back_animator->animaton()->start();
        x_ = 20;
        y_ = 108;
        position_ = 19;
        QObject::connect(back_animator->animaton(), SIGNAL(finished()), this, SIGNAL(finished_back_animation()));
    }
    else if(position_ == 36)
    {
        back_animator->animaton()->setStartValue(QPointF(x_, y_));
        back_animator->animaton()->setEndValue(QPointF(900, 284));
        back_animator->animaton()->start();
        x_ = 900;
        y_ = 284;
        position_ = 33;
        QObject::connect(back_animator->animaton(), SIGNAL(finished()), this, SIGNAL(finished_back_animation()));
    }
}

//this function animates player when player has to go to the jail, whether because of a card or because player steps on go to jail field
void Animate_Player::go_to_jail()
{
    Animate_Pixmap *jail_animator = new Animate_Pixmap(token_pixmap_);
    qint16 adjust = 0;
    if(!str_.contains("ship"))
    {
        adjust = 15;
    }

    jail_animator->animaton()->setDuration(qAbs(10 - position_) * 500);

    if(0 <= position_ && position_ <= 9)
    {        
        jail_animator->animaton()->setStartValue(QPointF(x_, y_));
        jail_animator->animaton()->setEndValue(QPointF(20, 910+adjust));
        jail_animator->animaton()->start();
        x_ = 20;
        y_ = 910 + adjust;
        position_ = 10;
        QObject::connect(jail_animator->animaton(), SIGNAL(finished()), this, SIGNAL(finished_jail_animation()));
    }
    else if(11 <= position_ && position_ <= 20)
    {
        jail_animator->animaton()->setStartValue(QPointF(x_, y_));
        jail_animator->animaton()->setEndValue(QPointF(20, 910+adjust));
        jail_animator->animaton()->start();
        x_ = 20;
        y_ = 910 + adjust;
        position_ = 10;
        QObject::connect(jail_animator->animaton(), SIGNAL(finished()), this, SIGNAL(finished_jail_animation()));
    }
    else if(21 <= position_ && position_ <= 30)
    {
        jail_animator->animaton()->setStartValue(QPointF(x_, y_));
        jail_animator->animaton()->setKeyValueAt(0.5, QPointF(20, 10+adjust));
        jail_animator->animaton()->setEndValue(QPointF(20, 910+adjust));
        jail_animator->animaton()->start();
        x_ = 20;
        y_ = 910 + adjust;
        position_ = 10;
        QObject::connect(jail_animator->animaton(), SIGNAL(finished()), this, SIGNAL(finished_jail_animation()));
    }
    else if(31 <= position_ && position_ <= 39)
    {
        jail_animator->animaton()->setStartValue(QPointF(x_, y_));
        jail_animator->animaton()->setKeyValueAt(0.2, QPointF(900, 10+adjust));
        jail_animator->animaton()->setKeyValueAt(0.6, QPointF(20, 10+adjust));
        jail_animator->animaton()->setEndValue(QPointF(20, 910+adjust));
        jail_animator->animaton()->start();
        x_ = 20;
        y_ = 910 + adjust;
        position_ = 10;
        QObject::connect(jail_animator->animaton(), SIGNAL(finished()), this, SIGNAL(finished_jail_animation()));
    }

}


